import React from 'react';
import './index.css';

const barData=[
  {
    name:'Launch Years',
    key:'year',
    years:[
    {
      0:2006,
      1:2007
    },
    {
      0:2008,
      1:2009
    },
    {
      0:2010,
      1:2011
    },
    {
      0:2012,
      1:2013
    },
    {
      0:2014,
      1:2015
    },
    {
      0:2016,
      1:2017
    },{
      0:2018,
      1:2019
    },
    {
      0:2020,
    },
  ]
  },
  {
    name: 'Successfully Launch',
    key: 'success_launch',
    years:[
      {
        0:`True`,
        1:`False`
      }
    ]
  },
  {
    name: 'Successfully Landing',
    key:"success_landing",
    years:[
      {
        0:`True`,
        1:`False`
      }
    ]
  }

  

]

class Grid extends React.Component {
    render() {
      return (
        <div className="grid-container">
          <div className="side-bar">
            <h4>Filters</h4>
            {barData.map(e=>(
              <div>
              <p>{e.name}</p>
              {e.years.map((y,i)=>(
                <div className="button-class">
                  <button onClick={()=>this.props.onChange(y[0], e.key)}>{y[0]}</button>
                 {y[1] && <button  onClick={()=>this.props.onChange(y[1], e.key)}>{y[1]}</button>} 
              </div>
              ))}
            </div>
            ))}
          </div>
          
          <div className="content-container">
            {this.props.data.map(e =>(
              <div className="grid-cover ">
            <img src={e.links.mission_patch} alt="not found"/>
            <p className="mission-name">{e.mission_name}</p>
            <div className="image-info">
              <p>Mission Ids : </p>
              <ul>
                {e.mission_id && e.mission_id.length > 0 && e.mission_id.map(id=>(
                  <li>{id}</li>
                ))}
              </ul>
            </div>
            <div className="image-info">
              <p>Launch Year : <span>{e.launch_year}</span></p>
            </div>
            <div className="image-info">
              <p>SuccessfullyLaunch: <span>{e.launch_success}</span></p>
            </div>
            <div className="image-info">
              <p>Successfully Landing: <span>{e.launch_landing ? e.launch_landing:''}</span></p>
              
            </div>
          </div>
            ))}
          
          </div>
          {this.props.data.map(e =>(
              <div className="grid-cover another-class">
            <img src={e.links.mission_patch} alt="not found"/>
            <p className="mission-name">{e.mission_name}</p>
            <div className="image-info">
              <p>Mission Ids : </p>
              <ul>
                {e.mission_id && e.mission_id.length > 0 && e.mission_id.map(id=>(
                  <li>{id}</li>
                ))}
              </ul>
            </div>
            <div className="image-info">
              <p>Launch Year : <span>{e.launch_year}</span></p>
            </div>
            <div className="image-info">
              <p>SuccessfullyLaunch: <span>{e.launch_success}</span></p>
            </div>
            <div className="image-info">
              <p>Successfully Landing: <span>{e.launch_landing ? e.launch_landing:''}</span></p>
              
            </div>
          </div>
            ))}
          
       </div>
       
      );
    }
  }
  
export default Grid;