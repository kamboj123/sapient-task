import React from 'react';
import Grid from './GridComp/Index';
import './index.css';


class GridComp extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            data:[],
            filters:{
                year: 0,
                success_launch: undefined,
                success_landing: undefined
            }
         };
      }
    getData(url){
        fetch(url)
        .then(res=>{
            res.json().then(result=>{
                this.setState(()=>({
                    data:result
                }))
            })
        })
    }

    onChange(val,key){
        this.setState(()=>({
            filters:{
                ...this.state.filters,
                [key]:val
            }
        }))
        
        
    }
    componentDidMount(){
        this.getData(`https://api.spaceXdata.com/v3/launches?limit=100`)
    }
    componentDidUpdate(prevProps, prevState){
        const cs = this.state.filters
        const ps = prevState.filters
        if(cs.year !== ps.year && cs.success_launch !== ps.success_launch && cs.success_landing !== ps.success_landing){
            this.getData(`https://api.spaceXdata.com/v3/launches?limit=100&launch_success=${cs.success_launch}&land_success=${cs.success_landing}&launch_year=${cs.year}
            `)
        } else if(cs.year!== ps.year){
            console.log("year")
            this.getData(`https://api.spaceXdata.com/v3/launches?limit=100&launch_year=${cs.year}
            `)
        } else if(cs.success_landing!== ps.success_landing && cs.success_launch!==ps.success_launch){
            this.getData(`https://api.spaceXdata.com/v3/launches?limit=100&land_success=${cs.success_landing}
            `)
        } else if(cs.success_launch!==ps.success_launch){
            this.getData(`https://api.spaceXdata.com/v3/launches?limit=100&launch_success=${cs.success_launch}
            `)
        }
    }
    render() {
      return (
        <div className="main-container">
            <h1>SpaceX Launch Programs</h1>
            <Grid data={this.state.data} onChange={(v, k)=>this.onChange(v, k)}/>
            <p><b>Developed by-</b> Baljeet kaur</p>
        </div>
      );
    }
  }
  
export default GridComp;